#!/bin/bash
rm -r ~/.portcli
rm -r ~/.portd

portd init mynode --chain-id port

portcli config keyring-backend test

portcli keys add me
portcli keys add you

portd add-genesis-account $(portcli keys show me -a) 1000token,100000000stake
portd add-genesis-account $(portcli keys show you -a) 1token

portcli config chain-id port
portcli config output json
portcli config indent true
portcli config trust-node true

portd gentx --name me --keyring-backend test
portd collect-gentxs